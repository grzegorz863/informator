﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Informator.model.entity
{
	class Information
	{
		private int id;
		private DateTime dateTime = new DateTime();
		private string text = string.Empty;

		public Information(int _id, DateTime _term, string _text)
		{
			this.id = _id;
			this.dateTime = _term;
			this.text = _text;
		}

		public Information(DateTime _term, string _text)
		{
			this.id = -1;
			this.dateTime = _term;
			this.text = _text;
		}

		public int ID
		{
			get { return this.id; }
		}

		public string Text
		{
			get { return this.text; }
			set { this.text = value; }
		}

		public DateTime DateTime
		{
			get	{ return this.dateTime; }
		}

		public string Date
		{
			get { return dateTime.Year + "." + dateTime.Month + "." + dateTime.Day; }
		}

		public string Time
		{
			get { return dateTime.Hour + ":" + dateTime.Minute + ":" + dateTime.Second; }
		}

	}
}
