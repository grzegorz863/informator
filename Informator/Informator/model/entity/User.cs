﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Informator.model.entity
{
	class User
	{
		private int id; 
		private string login = string.Empty;
		private string password = string.Empty;

		public User(int _id, string _login, string _password)
		{
			this.id = _id;
			this.login = _login;
			this.password = _password;
		}

		public int ID
		{
			get { return this.id; }
		}

		public string Login
		{
			get { return this.login; }
		}
	}
}
