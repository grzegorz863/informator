﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Informator.model.entity;

namespace Informator.model
{
	
	internal class ListEnumerator : IEnumerator<Information>
	{
		private List<Information> list;
		private Information currentInformation = default(Information);
		private int counter = default(int);

		public ListEnumerator(List<Information> list)
		{
			this.list = list;
		}

		public Information Current
		{
			get
			{
				if(currentInformation == default(Information))
					throw new InvalidOperationException("Use MoveNext before calling Current");

				return this.currentInformation;
			}
		}

		object IEnumerator.Current
		{
			get
			{
				if (currentInformation == default(Information))
					throw new InvalidOperationException("Use MoveNext before calling Current");

				return this.currentInformation;
			}
		}

		public void Dispose()
		{
			
		}

		public bool MoveNext()
		{
			if (list.Any())
			{
				if (list.Count > counter)
				{
					this.currentInformation = list[counter];
					counter++;
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		public void Reset()
		{
			this.counter = 0;
		}
	}
}
