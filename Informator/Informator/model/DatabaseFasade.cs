﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Informator.model.database;
using Informator.model.entity;
using System.Windows;

namespace Informator.model
{
	class DatabaseFasade
	{

		public User ValidateUser(string login, string password)
		{
			using (InformatorDBEntities db = new InformatorDBEntities())
			{
				db.Database.Connection.Open();
				var users = db.Users.Where(u => u.LOGIN == login && u.PASSWORD == password).FirstOrDefault();

				if (users == default(Users))
				{
					return null;
				}
				else
				{
					return new User(users.ID, login, password);
				}
			}
		}

		public bool RegistrationUser(string login, string password)
		{
			using (InformatorDBEntities db = new InformatorDBEntities())
			{
				db.Database.Connection.Open();

				var users = db.Users.Where(u => u.LOGIN == login).FirstOrDefault();
				if (users == default(Users))
				{
					Users user = new Users();
					user.LOGIN = login;
					user.PASSWORD = password;
					db.Users.Add(user);
					db.SaveChanges();
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public List<Information> GetUsersInformations(User _user)
		{
			using (InformatorDBEntities db = new InformatorDBEntities())
			{
				db.Database.Connection.Open();
				var usersInformations = db.Informations.Where(i => i.USER_ID == _user.ID);

				List<Information> list = new List<Information>();
				foreach(Informations info in usersInformations)
				{
					list.Add(new Information(info.ID, info.DATE, info.INFORMATION));
				}

				return list;
			}
		}

		public void AddNewInformation(User _user, Information _information)
		{
			using (InformatorDBEntities db = new InformatorDBEntities())
			{
				db.Database.Connection.Open();

				Informations informations = new Informations();
				informations.USER_ID = _user.ID;
				informations.DATE = _information.DateTime;
				informations.INFORMATION = _information.Text;
				db.Informations.Add(informations);
				db.SaveChanges();
			}
		}

		public void DeleteInformation(User _user, Information _information)
		{
			using (InformatorDBEntities db = new InformatorDBEntities())
			{
				db.Database.Connection.Open();

				Informations informations = db.Informations.Single(i => i.ID == _information.ID);
				db.Informations.Remove(informations);
				db.SaveChanges();
			}
		}
	}

	class SynchronizationDB
	{
		public bool SyncDB(ListOfInformations informationsToAdd, ListOfInformations listOfInformationsInDatabase, ListOfInformations informationsToDelete, User _user)
		{
			DatabaseFasade databaseFasade = new DatabaseFasade();
			foreach (var info in informationsToAdd)
			{
				databaseFasade.AddNewInformation(_user, info);
				listOfInformationsInDatabase.AddLast(info);
			}
			informationsToAdd.ClearList();

			foreach (var info in informationsToDelete)
			{
				databaseFasade.DeleteInformation(_user, info);
			}
			informationsToDelete.ClearList();

			return true;
		}
	}	
}