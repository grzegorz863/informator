﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Informator.model.entity;

namespace Informator.model
{
	class TimeVerification
	{
		public Information NotificationTimeVerification(ListOfInformations listOfInformationsInDatabase, ListOfInformations informationsToDelete, ListOfInformations informationsToAdd, User _user)
		{
			Information firstInformation = default(Information);
			const int FIRST_ELEMENT = 0;
			DateTime dateNow = DateTime.Now;

			if (!listOfInformationsInDatabase.IsEmpty)
			{
				firstInformation = listOfInformationsInDatabase.ShowElement(FIRST_ELEMENT);
				if (firstInformation.DateTime <= dateNow)
				{
					listOfInformationsInDatabase.RemoveFromList(firstInformation);
					informationsToDelete.AddLast(firstInformation);
					new SynchronizationDB().SyncDB(informationsToAdd, listOfInformationsInDatabase, informationsToDelete, _user);
					return firstInformation;
				}
			}
			if (!informationsToAdd.IsEmpty)
			{
				firstInformation = informationsToAdd.ShowElement(FIRST_ELEMENT);
				if (firstInformation.DateTime <= dateNow)
				{
					informationsToAdd.RemoveFromList(firstInformation);
					return firstInformation;
				}
			}

			return null;
		}
	}
}
