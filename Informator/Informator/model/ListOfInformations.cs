﻿using Informator.model.entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Informator.model
{

	class ListOfInformations : IEnumerable<Information>
	{
		private List<Information> list;

		public ListOfInformations(List<Information> _list)
		{
			this.list = _list;
		}

		public int NumberOfElements
		{
			get
			{
				return list.Count;
			}
		}

		public bool IsEmpty
		{
			get
			{
				return !list.Any();
			}
		}

		public void Sort()
		{
			list.Sort((x, y) => x.DateTime.CompareTo(y.DateTime));
		}

		public void AddLast(Information i)
		{
			list.Add(i);
		}

		public void RemoveFromList(Information i)
		{
			list.Remove(i);
		}

		public void ClearList()
		{
			list.Clear();
		}

		public void ConnectWithOtherList(List<Information> _list2)
		{
			list.AddRange(_list2);
		}

		public Information ShowElement(int index)
		{
			return list[index];
		}


		public IEnumerator<Information> GetEnumerator()
		{
			return new ListEnumerator(this.list);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return new ListEnumerator(this.list);
		}
	}
}
