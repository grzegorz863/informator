﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Informator.model
{
	internal class PassingClass
	{
		public ListOfInformations NewListOfInformations { get; set; }
		public ListOfInformations InformationsToDelete { get; set; }
	}
}
