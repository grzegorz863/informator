﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Informator.model.exceptions
{
	class WrongDateTimeFormat : System.Exception
	{
		private string message = string.Empty;

		public WrongDateTimeFormat(string _message)
		{
			this.message = _message;
		}

		public override string Message
		{
			get
			{
				return this.message;
			}
		}
	}
}
