﻿using Informator.model.exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Informator.model
{
	class DateValidation
	{

		public DateTime GetDate(DatePicker _date, string _hour, string _minute, string _second)
		{
			int hour, minute, second;
			DateTime? date = _date.SelectedDate;

			if (!date.HasValue)
				throw new WrongDateTimeFormat("Wybierz datę!");

			DateTime selectedDate = date.Value;

			if (int.TryParse(_hour, out hour))
			{
				if (hour >= 24 || hour <= 0)
					throw new WrongDateTimeFormat("Błędna godzina!");
			}
			else
				throw new WrongDateTimeFormat("Błędna godzina!");

			if (int.TryParse(_minute, out minute))
			{
				if (minute >= 60 || minute < 0)
					throw new WrongDateTimeFormat("Błędna minuta!");
			}
			else
				throw new WrongDateTimeFormat("Błędna minuta!");

			if (int.TryParse(_second, out second))
			{
				if (second >= 60 || second < 0)
					throw new WrongDateTimeFormat("Błędna sekunda!");
			}
			else
				throw new WrongDateTimeFormat("Błędna sekunda!");

			return new DateTime(selectedDate.Year, selectedDate.Month, selectedDate.Day, hour, minute, second);
		}
	}
}
