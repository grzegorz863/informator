﻿using Informator.model;
using Informator.model.entity;
using Informator.model.exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Informator.view
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private DispatcherTimer timer = new DispatcherTimer();
		private ListOfInformations listOfInformationsInDatabase;
		private ListOfInformations informationsToAdd = new ListOfInformations(new List<Information>());
		private ListOfInformations informationsToDelete = new ListOfInformations(new List<Information>());
		private User user;

		public MainWindow(object _user)
		{
			InitializeComponent();

			this.user = (User)_user;
			DatabaseFasade databaseFasade = new DatabaseFasade();
			listOfInformationsInDatabase = new ListOfInformations(databaseFasade.GetUsersInformations(this.user));
			listOfInformationsInDatabase.Sort();
			DP_date.SelectedDate = DateTime.Today;
			timer.Tick += new EventHandler(Timer_Click);
			timer.Interval = new TimeSpan(0, 0, 1);
			timer.Start();
		}

		private void Timer_Click(object sender, EventArgs e)
		{
			TB_current_date.Text = DateTime.Now.ToString();
			Information information = new TimeVerification().NotificationTimeVerification(listOfInformationsInDatabase, informationsToDelete, informationsToAdd, this.user);
			if (information !=null)
				this.ShowNotification(information);
		}

		private void ShowNotification(Information _information)
		{		
			Notification notification = new Notification(_information);
			notification.Show();
			SystemSounds.Beep.Play();
		}

		private void B_confirm_Click(object sender, RoutedEventArgs e)
		{
			DateValidation dateValidation = new DateValidation();
			try
			{
				DateTime selectedDate = dateValidation.GetDate(DP_date, TB_hour.Text, TB_minute.Text, TB_seconds.Text);
				Information information = new Information(selectedDate, TB_info.Text);
				informationsToAdd.AddLast(information);
				informationsToAdd.Sort();
				TB_hour.Clear();
				TB_minute.Clear();
				TB_seconds.Clear();
				TB_info.Clear();
			}
			catch(WrongDateTimeFormat error)
			{
				MessageBox.Show(error.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private void B_show_info_Click(object sender, RoutedEventArgs e)
		{
			Preview preview = new Preview(listOfInformationsInDatabase, informationsToAdd);
			preview.Show();
			preview.Closed += Preview_Closed;
			this.IsEnabled = false;
		}

		private void B_cancel_Click(object sender, RoutedEventArgs e)
		{
			MessageBoxResult result = MessageBox.Show("Czy napewno chcesz wyjść? Po wyjściu dane zostaną utracone!", "Wyjście", MessageBoxButton.YesNo, MessageBoxImage.Warning);
			if (result == MessageBoxResult.Yes)
				this.Close();
		}

		private void B_save_and_close_Click(object sender, RoutedEventArgs e)
		{
			MessageBoxResult result = MessageBox.Show("Czy napewno chcesz się wylogować? Po wylogownaiu dane zostaną zapisane!", "Wylogowywanie", MessageBoxButton.YesNo, MessageBoxImage.Warning);
			if (result == MessageBoxResult.Yes)
			{
				new SynchronizationDB().SyncDB(informationsToAdd, listOfInformationsInDatabase, informationsToDelete, this.user);
				this.Close();
				Application.Current.Windows[0].Close();
			}
		}

		private void B_Logout_Click(object sender, RoutedEventArgs e)
		{
			MessageBoxResult result = MessageBox.Show("Czy napewno chcesz się wylogować? Po wylogownaiu dane zostaną zapisane!", "Wylogowywanie", MessageBoxButton.YesNo, MessageBoxImage.Warning);
			if (result == MessageBoxResult.Yes)
			{
				new SynchronizationDB().SyncDB(informationsToAdd, listOfInformationsInDatabase, informationsToDelete, this.user);
				this.Close();
			}
		}

		private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			this.timer.Tick -= Timer_Click;
			Application.Current.Windows[0].Visibility = Visibility.Visible;
		}

		private void Preview_Closed(object sender, EventArgs e)
		{
			Preview preview = (Preview)sender;
			PassingClass pass = (PassingClass) preview.DataContext;
			informationsToDelete = pass.InformationsToDelete;
			informationsToAdd = pass.NewListOfInformations;
		}

		private void B_sync_Click(object sender, RoutedEventArgs e)
		{
			if(new SynchronizationDB().SyncDB(informationsToAdd, listOfInformationsInDatabase, informationsToDelete, this.user))
				MessageBox.Show("Zsynchronizowano z bazą danych na serwerze", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);
		}

		private void TB_hour_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (TB_hour.Text.Length >= 2)
				TB_minute.Focus();

			if(TB_hour.Text.Length >2)
				TB_hour.Text = TB_hour.Text.Remove(2, 1);
		}

		private void TB_minute_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (TB_minute.Text.Length == 2)
				TB_seconds.Focus();

			if (TB_minute.Text.Length > 2)
				TB_minute.Text = TB_minute.Text.Remove(2, 1);
		}

		private void TB_seconds_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (TB_seconds.Text.Length > 2)
				TB_seconds.Text = TB_seconds.Text.Remove(2, 1);
		}

		private void DP_date_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
		{
			DateTime dateTime = DP_date.SelectedDate.Value;

			if (dateTime < DateTime.Today)
			{
				DP_date.SelectedDate = DateTime.Today;
				MessageBox.Show("Wybierz dzisiejszą lub przyszłą datę", "Błędna data", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
	}
}
