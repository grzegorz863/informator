﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Informator.model;

namespace Informator.view
{
	/// <summary>
	/// Interaction logic for Registration.xaml
	/// </summary>
	public partial class Registration : Window
	{
		public Registration()
		{
			InitializeComponent();
		}

		private void B_Reg_Click(object sender, RoutedEventArgs e)
		{
			if (PB_Reg_Pass.Password == PB_Reg_RPass.Password)
			{
				string login = TB_Reg_Login.Text;
				string password = PB_Reg_Pass.Password;

				DatabaseFasade databaseFasade = new DatabaseFasade();

				if (databaseFasade.RegistrationUser(login, password))
				{
					this.Close();
					MessageBox.Show("Zarejestrowano", "OK", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
				}
				else
				{
					PB_Reg_Pass.Password = string.Empty;
					PB_Reg_RPass.Password = string.Empty;
					TB_Reg_Login.Text = string.Empty;
					MessageBox.Show("Użytkownik o podanym loginie już istnieje!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
				}
			}
			else
			{
				PB_Reg_Pass.Password = string.Empty;
				PB_Reg_RPass.Password = string.Empty;
				MessageBox.Show("Hasła są różne", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
			}
			
		}

		private void OnClose(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Application.Current.Windows[0].IsEnabled = true;
		}

		private void button_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}
	}
}
