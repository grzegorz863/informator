﻿using Informator.model;
using Informator.model.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Informator.view
{
	/// <summary>
	/// Interaction logic for Preview.xaml
	/// </summary>
	public partial class Preview : Window
	{
		private ListOfInformations listOfInformationsInDatabase;
		private ListOfInformations newListOfInformations;
		private ListOfInformations listOfInformationsToDelete = new ListOfInformations(new List<Information>());
		private int numbersOfInformatationsInDatabase;

		public Preview(object _listOfInformationsInDatabase, object _newListOfInformations)
		{
			InitializeComponent();
			listOfInformationsInDatabase = (ListOfInformations)_listOfInformationsInDatabase;
			newListOfInformations = (ListOfInformations)_newListOfInformations;
			numbersOfInformatationsInDatabase = listOfInformationsInDatabase.NumberOfElements;
			
			foreach(var info in listOfInformationsInDatabase)
			{
				listView.Items.Add(info);
			}

			foreach (var info in newListOfInformations)
			{
				listView.Items.Add(info);
			}
		}

		private void B_OK_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void Preview_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Application.Current.Windows[1].IsEnabled = true;
			PassingClass pass = new PassingClass { NewListOfInformations=newListOfInformations, InformationsToDelete= listOfInformationsToDelete };
			this.DataContext = pass;
		}

		private void B_delete_Click(object sender, RoutedEventArgs e)
		{
			if (listView.SelectedItem != null)
			{
				Information selectedItem = (Information) listView.SelectedItem;
				int selectedIndex = listView.SelectedIndex;
				if (selectedItem.ID != -1)
				{
					listOfInformationsToDelete.AddLast(selectedItem);
					listOfInformationsInDatabase.RemoveFromList(selectedItem);
				}
				else
					newListOfInformations.RemoveFromList(selectedItem);

				listView.Items.Remove(listView.SelectedItem);
			}
			else
				MessageBox.Show("Najpierw wybierz element do usunięcia!", "Informacja", MessageBoxButton.OK, MessageBoxImage.Information);	
		}
	}
}
