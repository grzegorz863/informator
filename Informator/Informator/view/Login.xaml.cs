﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Informator.model;
using Informator.model.entity;

namespace Informator.view
{
	/// <summary>
	/// Interaction logic for Login.xaml
	/// </summary>
	public partial class Login : Window
	{
		DatabaseFasade databaseFasade = new DatabaseFasade();
		User user;
		public Login()
		{
			InitializeComponent();
		}

		private void B_OK_Click(object sender, RoutedEventArgs e)
		{

			string login = textBox.Text;
			string password = passwordBox.Password;

			this.user = databaseFasade.ValidateUser(login, password);

			if(user == null)
			{
				MessageBox.Show("Nie ma takiego użytkownika", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
			}
			else
			{
				MainWindow mainWindow = new MainWindow((object) user);
				mainWindow.Show();
				this.Visibility = Visibility.Hidden;
			}
		}

		private void B_Cancel_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void B_Reg_Click(object sender, RoutedEventArgs e)
		{
			Registration reg = new Registration();
			reg.Show();
			this.textBox.Clear();
			this.passwordBox.Clear();
			this.IsEnabled = false;
		}

		private void Login_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			System.Environment.Exit(1);
		}
	}
}
